INTRODUCTION
------------


The Pikaday module adds the Pikaday calendar popup to your site's pages. 
It includes the moment.js library as well since Pikaday works better with
it.

[Pikaday](https://dbushell.com/Pikaday/) is written by David Bushell and can be found at
https://dbushell.com/Pikaday/

This module was developed to provide a calendar for filtering dates in 
views using the Views Database Connector (VDC) module. The Date module, which is the normal
solution, does not work with VDC to provide a calendar popup for filtering dates. 

While it would be best to make Date work with VDC, the more expedient
solution was to use a js library like Pikaday. And I like Pikaday.


RECOMMENDED MODULES
-------------------

 * [Views Database Connector](https://www.drupal.org/project/views_database_connector)

 * [Markdown filter](https://www.drupal.org/project/markdown) :
   When enabled, display of the project's README.md help will be rendered
   with markdown.


INSTALLATION
-----------


* Install as you would normally install a contributed Drupal module.
See:
   [Installing contributed modules](https://drupal.org/documentation/install/modules-themes/modules-7)
   for further information.


CONFIGURATION
-------------


The module has no menu or modifiable settings.


EXAMPLE
-------


Create a view using VDC with a date filter. Examine the filter's inputs using Developer Tools 
to determine the DOM names. For instance an input filter named submissiondate might have
inputs with names submissiondate[value], submissiondate[min] and submissiondate[max].

Add some javascript to your view using a **Global : Text area** in the header or footer. Set the text
format to *Full HTML* and check **Display even if the view has no result**. 

The javascript to add the Pikaday calendar popup to your date filter
inputs might look like this:

~~~html
<script>
    jQuery(function () {
        jQuery('input[name^="submissiondate"]').each(function () {
            try {
                var dPicker = new Pikaday({
                    numberOfMonths: 2,
                    field: jQuery(this)[0],
                    format: 'D MMM YYYY',
                    firstDay: 1,
                    minDate: new Date(1950, 0, 1),
                    maxDate: new Date(2050, 12, 31),
                    yearRange: [1950, 2050]
                });
            } catch (e) {
                console.log('The Pikaday library is not loaded!');
            }
        });
    });
</script>
~~~

